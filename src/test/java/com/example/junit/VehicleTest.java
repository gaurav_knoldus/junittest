package com.example.junit;

import com.example.junit.entity.Vehicle;
import com.example.junit.service.VehicleService;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleTest {
    @Autowired
    MockMvc mockMvc;

    @Mock
    VehicleService vehicleService;

    @Test
    public void get_allVehicles_returnsOkWithListOfVehicles() throws Exception {

        List<Vehicle> vehicleList = new ArrayList<>();
        Vehicle vehicle1 = new Vehicle("AD23E5R98EFT3SL00", "Ford", "Fiesta", 2016, false);
        Vehicle vehicle2 = new Vehicle("O90DECADE564W4W83", "Volkswagen", "Jetty", 2016, false);
        vehicleList.add(vehicle1);
        vehicleList.add(vehicle2);
        /* Mocking out the vehicle service */
        Mockito.when(vehicleService.getAllVehicles()).thenReturn(vehicleList);
        mockMvc.perform(MockMvcRequestBuilders.post("/demo/vehicles"))
                .andExpect(status().isOk());


    }



}
