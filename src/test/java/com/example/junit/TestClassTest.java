package com.example.junit;

import com.example.junit.repository.DataServiceRepository;
import com.example.junit.service.serviceimpl.SomeBusinessImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestClassTest {

    @Mock
    private DataServiceRepository dataServiceRepositoryMock;

    @InjectMocks
    private SomeBusinessImpl businessImpl;

    TestClass test = new TestClass();

    @Test
    public void sum_with3numbers() {
        assertEquals(6, test.sum(new int[]{1, 2, 3}));
    }

    @Test
    public void sum_with1number() {
        assertEquals(3, test.sum(new int[]{3}));
    }

    @Test
    public void testFindTheGreatestFromAllData() {
        when(dataServiceRepositoryMock.retrieveAllData()).thenReturn(new int[]{24, 15, 3});
        int result = businessImpl.findTheGreatestFromAllData();
        assertEquals(24, result);
    }

    @Test
    public void testFindTheGreatestFromAllData_NoValues() {
        when(dataServiceRepositoryMock.retrieveAllData()).thenReturn(new int[]{});
        assertEquals(Integer.MIN_VALUE, businessImpl.findTheGreatestFromAllData());
    }


}
