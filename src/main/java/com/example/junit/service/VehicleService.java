package com.example.junit.service;

import com.example.junit.entity.Vehicle;
import java.util.List;

public interface VehicleService {

    List<Vehicle> getAllVehicles();
}
