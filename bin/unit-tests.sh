#!/bin/bash

# This script runs the unit tests in Bitbucket pipelines (see `bitbucket-pipeline.yml`).
# The AWS variables must exist as environment variables in Bitbucket.

./mvnw clean test